from django.contrib import admin
from .models import Category, Color, WoodType, Size, Product, Order

admin.site.register(Category)
admin.site.register(Color)
admin.site.register(WoodType)
admin.site.register(Size)
admin.site.register(Product)
admin.site.register(Order)

