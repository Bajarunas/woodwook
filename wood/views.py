from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Category, Product, Color, WoodType, Size, Order
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import HttpResponseRedirect
from .filters import ProductFilter
from django.views.generic import View
from django.core.mail import send_mail
from django.utils import  translation



def superuser_required():
    def wrapper(wrapped):
        class WrappedClass(UserPassesTestMixin, wrapped):
            def test_func(self):
                return self.request.user.is_superuser

        return WrappedClass
    return wrapper


def home(request):
    return render(request, 'wood/home.html')


def about(request):
    return render(request, 'wood/information/about.html')


def contacts(request):
    return render(request, 'wood/information/contacts.html')

    
def services(request):
    return render(request, 'wood/information/services.html')


class StairListView(ListView):
    model = Product
    template_name = 'wood/stairs/stairs.html'
    context_object_name = 'Product'
    ordering = ['-id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = ProductFilter(self.request.GET, queryset=self.get_queryset())
        return context

        
class StairDetailView(DetailView):
    model = Product
    template_name = 'wood/stairs/stairs_detail.html'


@superuser_required()
class ProductCreateView(SuccessMessageMixin, CreateView):
    model = Product
    fields = ['category', 'title', 'title_en', 'description', 'description_en', 'color', 'wood', 'size', 'image', 'cost']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/create.html'
    success_url = '/products/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        print(language)
        if language == 'lt':
            return 'Produktas %(title)s sukurtas!' % {'title': self.object.title}
        else:
            return 'Product %(title)s was created!' % {'title': self.object.title_en}


@superuser_required()
class ProductUpdateView(SuccessMessageMixin, UpdateView):
    model = Product
    fields = ['category', 'title', 'title_en', 'description', 'description_en', 'color', 'wood', 'size', 'image', 'cost']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/create.html'
    success_url = '/products/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Produktas %(title)s pakeistas!' % {'title': self.object.title}
        else:
            return 'Product %(title)s was updated!' % {'title': self.object.title_en}



@superuser_required()
class ProductDeleteView(SuccessMessageMixin, DeleteView):
    model = Product
    success_url = '/products/'
    success_message = "Produktas ištrintas!"
    template_name = 'wood/common/delete.html'

    def delete(self, request, *args, **kwargs):
        language = translation.get_language()
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            if language == 'lt':
                messages.add_message(request, messages.SUCCESS, 'Ištrinta sėkmingai!')
            else:
                messages.add_message(request, messages.SUCCESS, 'Deleted succesfully!')
        except ProtectedError:
            messages.add_message(request, messages.warning, 'Nepavyko ištrinti!')
        return HttpResponseRedirect(success_url)


class DoorsListView(ListView):
    model = Product
    template_name = 'wood/doors/doors.html'
    context_object_name = 'Product'
    ordering = ['-id']
    filterset_class = ProductFilter
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = ProductFilter(self.request.GET, queryset=self.get_queryset())
        return context


class DoorsDetailView(DetailView):
    model = Product
    template_name = 'wood/doors/doors_detail.html'


class GalleryListView(ListView):
    model = Product
    template_name = 'wood/gallery/gallery.html'
    context_object_name = 'Product'


@superuser_required()
class ProductTvs(ListView):
    model = Product
    template_name = 'wood/tvs/products.html'
    context_object_name = 'Product'
    ordering = ['-id']
    paginate_by = 10


@superuser_required()
class CategoryTvs(ListView):
    model = Category
    template_name = 'wood/tvs/category.html'
    context_object_name = 'Category'
    ordering = ['-id']
    paginate_by = 10


@superuser_required()
class CategoryDeleteView(SuccessMessageMixin, DeleteView):
    model = Category
    success_url = '/category/'
    success_message = "Kategorija ištrinta!"
    template_name = 'wood/common/delete_category.html'

    def delete(self, request, *args, **kwargs):
        language = translation.get_language()
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            if language == 'lt':
                messages.add_message(request, messages.SUCCESS, 'Ištrinta sėkmingai!')
            else:
                messages.add_message(request, messages.SUCCESS, 'Deleted succesfully!')
        except ProtectedError:
            messages.add_message(request, messages.warning, 'Nepavyko ištrinti!')
        return HttpResponseRedirect(success_url)


@superuser_required()
class CategoryUpdateView(SuccessMessageMixin, UpdateView):
    model = Category
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/category_create.html'
    success_url = '/category/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Kategorija %(title)s pakeista!' % {'title': self.object.title}
        else:
            return 'Category %(title)s was updated!' % {'title': self.object.title_en}



@superuser_required()
class CategoryCreateView(SuccessMessageMixin, CreateView):
    model = Category
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/category_create.html'
    success_url = '/category/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Kategorija %(title)s sukurta!' % {'title': self.object.title}
        else:
            return 'Category %(title)s was created!' % {'title': self.object.title_en}




@superuser_required()
class ColorTvs(ListView):
    model = Color
    template_name = 'wood/tvs/color.html'
    context_object_name = 'Color'
    ordering = ['-id']
    paginate_by = 10




@superuser_required()
class ColorDeleteView(SuccessMessageMixin, DeleteView):
    model = Color
    success_url = '/color/'
    success_message = "Spalva ištrinta!"
    template_name = 'wood/common/delete_color.html'

    def delete(self, request, *args, **kwargs):
        language = translation.get_language()
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            if language == 'lt':
                messages.add_message(request, messages.SUCCESS, 'Ištrinta sėkmingai!')
            else:
                messages.add_message(request, messages.SUCCESS, 'Deleted succesfully!')
        except ProtectedError:
            messages.add_message(request, messages.warning, 'Nepavyko ištrinti!')
        return HttpResponseRedirect(success_url)


@superuser_required()
class ColorUpdateView(SuccessMessageMixin, UpdateView):
    model = Color
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/color_create.html'
    success_url = '/color/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Spalva %(title)s pakeista!' % {'title': self.object.title}
        else:
            return 'Color %(title)s was updated!' % {'title': self.object.title_en}


@superuser_required()
class ColorCreateView(SuccessMessageMixin, CreateView):
    model = Color
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/color_create.html'
    success_url = '/color/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Spalva %(title)s sukurta!' % {'title': self.object.title}
        else:
            return 'Color %(title)s was created!' % {'title': self.object.title_en}





@superuser_required()
class WoodTvs(ListView):
    model = WoodType
    template_name = 'wood/tvs/wood.html'
    context_object_name = 'Wood'
    ordering = ['-id']
    paginate_by = 10



@superuser_required()
class WoodDeleteView(SuccessMessageMixin, DeleteView):
    model = WoodType
    success_url = '/wood/'
    success_message = "Medžio tipas ištrintas!"
    template_name = 'wood/common/delete_wood.html'

    def delete(self, request, *args, **kwargs):
        language = translation.get_language()
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            if language == 'lt':
                messages.add_message(request, messages.SUCCESS, 'Ištrinta sėkmingai!')
            else:
                messages.add_message(request, messages.SUCCESS, 'Deleted succesfully!')
        except ProtectedError:
            messages.add_message(request, messages.warning, 'Nepavyko ištrinti!')
        return HttpResponseRedirect(success_url)



@superuser_required()
class WoodUpdateView(SuccessMessageMixin, UpdateView):
    model = WoodType
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/wood_create.html'
    success_url = '/wood/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Medžio tipas %(title)s pakeistas!' % {'title': self.object.title}
        else:
            return 'Wood type %(title)s was updated!' % {'title': self.object.title_en}



@superuser_required()
class WoodCreateView(SuccessMessageMixin, CreateView):
    model = WoodType
    fields = ['title', 'title_en', 'description', 'description_en']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/wood_create.html'
    success_url = '/wood/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Medžio tipas %(title)s sukurtas!' % {'title': self.object.title}
        else:
            return 'Wood type %(title)s was created!' % {'title': self.object.title_en}



@superuser_required()
class SizeTvs(ListView):
    model = Size
    template_name = 'wood/tvs/size.html'
    context_object_name = 'Size'
    ordering = ['-id']
    paginate_by = 10



@superuser_required()
class SizeDeleteView(SuccessMessageMixin, DeleteView):
    model = Size
    success_url = '/size/'
    success_message = "Dydis ištrintas!"
    template_name = 'wood/common/delete_size.html'

    def delete(self, request, *args, **kwargs):
        language = translation.get_language()
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            if language == 'lt':
                messages.add_message(request, messages.SUCCESS, 'Ištrinta sėkmingai!')
            else:
                messages.add_message(request, messages.SUCCESS, 'Deleted succesfully!')
        except ProtectedError:
            messages.add_message(request, messages.warning, 'Nepavyko ištrinti!')
        return HttpResponseRedirect(success_url)



@superuser_required()
class SizeUpdateView(SuccessMessageMixin, UpdateView):
    model = Size
    fields = ['title', 'title_en', 'description', 'description_en', 'height', 'width']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/size_create.html'
    success_url = '/size/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Dydis %(title)s pakeistas!' % {'title': self.object.title}
        else:
            return 'Size %(title)s was updated!' % {'title': self.object.title_en}



@superuser_required()
class SizeCreateView(SuccessMessageMixin, CreateView):
    model = Size
    fields = ['title', 'title_en', 'description', 'description_en', 'height', 'width']
    success_message = "%(calculated_field)s"
    template_name = 'wood/common/size_create.html'
    success_url = '/size/'

    def get_success_message(self, cleaned_data):
        language = translation.get_language()
        if language == 'lt':
            return 'Dydis %(title)s sukurtas!' % {'title': self.object.title}
        else:
            return 'Size %(title)s was created!' % {'title': self.object.title_en}




def order(request, pk):
    product = Product.objects.get(pk=pk)
    return render(request, "wood/order/order.html", {"product":product})



def order_product(request, pk):
    product = Product.objects.get(pk=pk)
    order.user = request.user
    n = Order.objects.create(product=product, user=order.user)
    Order.objects.filter(pk=n.id).update(title='Užsakymas '+ str(n.id), title_en='Order '+str(n.id))
    return render(request, "wood/order/order_product.html", {"product":product})



@superuser_required()
class OrderList(ListView):
    model = Order
    template_name = 'wood/order/order_list.html'
    context_object_name = 'Order'
    ordering = ['-id']
    paginate_by = 12

    def get_context_data(self, **kwargs):
        Order1 = Order.objects.all().order_by('-id')
        count = Order.objects.filter(confirm=None, finished=None, reject=None).count()
        print(count)
        if count == 0:
            return {'count': count}
        else:
            return {'Order1': Order1, 'count': count}


class OrderDetail(DetailView):
    model = Order
    template_name = 'wood/order/order_detail.html'
    context_object_name = 'Order'



def order_change(request, pk):
    order_title = Order.objects.get(pk=pk)
    language = request.LANGUAGE_CODE
    
    if request.GET.get('Approve'):
        Order.objects.filter(pk=pk).update(confirm=True)
        if language == 'lt':
            messages.success(request, str(order_title) +' priimtas!')
        else:
            messages.success(request, str(order_title.title_en) +' confirmed!')
        send_mail(
            'Jūsų užsakytos '+str(order_title.product)+ ' patvirtintos!',
            'Sveiki, peržiūrėjome jūsų užsakymą, ir nutarėme patvirtinti, užsakymas bus vykdomas!',
            'woodwork@gmail.lt',
            [order_title.user.email],
            fail_silently=False,
            )
        return redirect('active-orders')
    if request.GET.get('Reject'):
        Order.objects.filter(pk=pk).update(reject=True)
        if language == 'lt':
            messages.warning(request, str(order_title) +' atmestas!')
        else:
            messages.warning(request, str(order_title.title_en) +' rejected!')
        send_mail(
            'Jūsų užsakytos '+str(order_title.product)+ ' atmestas!',
            'Sveiki, peržiūrėjome jūsų užsakymą, ir nutarėme atmesti, užsakymas bus nevykdomas!',
            'woodwork@gmail.lt',
            [order_title.user.email],
            fail_silently=False,
            )
        if request.user.is_superuser:
            return redirect('active-orders')
        else:
            return redirect('profile')
    if request.GET.get('Finished'):
        Order.objects.filter(pk=pk).update(finished=True)
        if language == 'lt':
            messages.warning(request, str(order_title.product) +' užbaigtas!')
        else:
            messages.warning(request, str(order_title.product.title_en) +' finished!')
        return redirect('confirmed-orders')
    else:
        messages.warning(request, 'Kažkas neveikia! Bandykite dar kartą!')
        return redirect('active-orders')


@superuser_required()
class ConfirmedOrderList(ListView):
    model = Order
    template_name = 'wood/order/confirmed_order_list.html'
    context_object_name = 'Order'
    ordering = ['-id']
    paginate_by = 12

    def get_context_data(self, **kwargs):
        Order1 = Order.objects.all().order_by('-id')
        count = Order.objects.filter(confirm=True, finished=None).count()
        print(count)
        if count == 0:
            return {'count': count}
        else:
            return {'Order1': Order1, 'count': count}



def contact_send(request):
    language = request.LANGUAGE_CODE
    name = request.POST.get('name')
    email = request.POST.get('email')
    subject = request.POST.get('subject')
    message = request.POST.get('message')
    if name == "" or email == "" or subject == "" or message == "":
        print("patenka cia?")
        if language == 'lt':
            messages.warning(request, 'Prašau užpildyti visus laukelius!')
        else:
            messages.warning(request, 'Please fill all the fields!')
    else:
        send_mail(
            str(subject),
            str(message),
            email,
            ['bartaseviciutek@gmail.com'],
            fail_silently=False,
            )
        if language == 'lt':
            messages.success(request, 'Žinutė išsiųsta woodwork komandai! Peržiūrėję žinutę, pranešime jum per 2 - 4 darbo dienas.')
        else:
            messages.success(request, 'Message send to woodwork team! When we review the message, we will inform you in 2 - 4 work days.')
        
    return redirect('wood-contacts')