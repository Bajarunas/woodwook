from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import  translation


class Category(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    title_en = models.CharField(max_length=100)
    description_en = models.TextField(blank=True)

    def __str__(self):
        LANGUAGE_CODE = translation.get_language()
        if LANGUAGE_CODE == 'lt':
            return self.title
        else:
            return self.title_en

    def get_absolute_url(self):
        return reverse('tvs-category')

class Color(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    title_en = models.CharField(max_length=100)
    description_en = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('tvs-color')

    def __str__(self):
        LANGUAGE_CODE = translation.get_language()
        if LANGUAGE_CODE == 'lt':
            return self.title
        else:
            return self.title_en

class WoodType(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    title_en = models.CharField(max_length=100)
    description_en = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('tvs-wood')


    def __str__(self):
        LANGUAGE_CODE = translation.get_language()
        if LANGUAGE_CODE == 'lt':
            return self.title
        else:
            return self.title_en

class Size(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    title_en = models.CharField(max_length=100)
    description_en = models.TextField(blank=True)

    height = models.FloatField(null=True, blank=True, default=None)
    width = models.FloatField(null=True, blank=True, default=None)

    def __str__(self):
        LANGUAGE_CODE = translation.get_language()
        if LANGUAGE_CODE == 'lt':
            return self.title
        else:
            return self.title_en
    
    def get_absolute_url(self):
        return reverse('tvs-size')


class Product(models.Model):
    category = models.ForeignKey('Category', related_name='category_related', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    title_en = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    description_en = models.TextField(blank=True)
    color = models.ForeignKey('Color', related_name='color_related', on_delete=models.CASCADE)
    wood = models.ForeignKey('WoodType', related_name='wood_related', on_delete=models.CASCADE)
    size = models.ForeignKey('Size', related_name='size_related', on_delete=models.CASCADE)
    image = models.ImageField(default='default.png')
    cost = models.FloatField(null=True, blank=True, default=None)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('wood-home')
    


class Order(models.Model):
    title = models.CharField(max_length=100, null=True)
    title_en =  models.CharField(max_length=100, null=True)
    product = models.ForeignKey('Product', related_name='product_related', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='user_related', on_delete=models.CASCADE)
    confirm = models.BooleanField(null=True)
    reject = models.BooleanField(null=True)
    finished = models.BooleanField(null=True)

    def __str__(self):
        return self.title
        

    def get_absolute_url(self):
        return reverse('wood-home')