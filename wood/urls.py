from django.urls import path, include
from .views import (StairListView, StairDetailView, DoorsDetailView, DoorsListView,
GalleryListView, ProductCreateView, ProductUpdateView, ProductDeleteView,
ProductTvs, CategoryTvs, CategoryDeleteView, CategoryCreateView, CategoryUpdateView, 
ColorTvs, ColorDeleteView, ColorUpdateView, ColorUpdateView, ColorCreateView,
WoodTvs, WoodDeleteView, WoodUpdateView, WoodUpdateView, WoodCreateView,
SizeTvs, SizeDeleteView, SizeUpdateView, SizeUpdateView, SizeCreateView,
OrderList, ConfirmedOrderList, OrderDetail)
from . import views
from django.contrib.auth import views as auth_views
from django.conf.urls import url


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('', views.home, name='wood-home'),
    path('about/', views.about, name='wood-about'),
    path('stairs/', StairListView.as_view(), name='wood-stairs'),
    path('stairs/<int:pk>/', StairDetailView.as_view(), name='stairs-detail'),
    path('doors/', DoorsListView.as_view(), name='wood-doors'),
    path('doors/<int:pk>/', DoorsDetailView.as_view(), name='doors-detail'),
    path('contacts/', views.contacts, name='wood-contacts'),
    path('gallery/', GalleryListView.as_view(), name='wood-gallery'),   
    path('services/', views.services, name='wood-services'),
    path('product/new/', ProductCreateView.as_view(), name='product-create'),
    path('product/<int:pk>/update/', ProductUpdateView.as_view(), name='product-update'),
    path('product/<int:pk>/delete/', ProductDeleteView.as_view(), name='product-delete'),
    path('uzpiso_blet/', views.contact_send, name='contact-send'),



    #tvs
    path('products/', ProductTvs.as_view(), name='tvs-product'),

    path('category/', CategoryTvs.as_view(), name='tvs-category'),
    path('category/<int:pk>/delete/', CategoryDeleteView.as_view(), name='category-delete'),
    path('category/<int:pk>/update/', CategoryUpdateView.as_view(), name='category-update'),
    path('category/new/', CategoryCreateView.as_view(), name='category-create'),

    path('color/', ColorTvs.as_view(), name='tvs-color'),
    path('color/<int:pk>/delete/', ColorDeleteView.as_view(), name='color-delete'),
    path('color/<int:pk>/update/', ColorUpdateView.as_view(), name='color-update'),
    path('color/new/', ColorCreateView.as_view(), name='color-create'),


    path('wood/', WoodTvs.as_view(), name='tvs-wood'),
    path('wood/<int:pk>/delete/', WoodDeleteView.as_view(), name='wood-delete'),
    path('wood/<int:pk>/update/', WoodUpdateView.as_view(), name='wood-update'),
    path('wood/new/', WoodCreateView.as_view(), name='wood-create'),


    path('size/', SizeTvs.as_view(), name='tvs-size'),
    path('size/<int:pk>/delete/', SizeDeleteView.as_view(), name='size-delete'),
    path('size/<int:pk>/update/', SizeUpdateView.as_view(), name='size-update'),
    path('size/new/', SizeCreateView.as_view(), name='size-create'),


    path('order/<int:pk>/', views.order, name='order'),
    path('order_product/<int:pk>/', views.order_product, name='order-product'),
    path('active_orders/', OrderList.as_view(), name='active-orders'),
    path('order_change/<int:pk>/', views.order_change, name='order-change'),
    path('confirmed_orders/', ConfirmedOrderList.as_view(), name='confirmed-orders'),
    path('order_detail/<int:pk>/', OrderDetail.as_view(), name='order-detail'),

    url('^', include('django.contrib.auth.urls')),
]
