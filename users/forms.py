from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput
from .models import Profile
from django.utils.translation import get_language


class MyAuthForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username','password']
    def __init__(self, *args, **kwargs):
        super(MyAuthForm, self).__init__(*args, **kwargs)
        language = get_language()
        if language == 'en':
            self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'})
            self.fields['username'].label = False
            self.fields['password'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Password'}) 
            self.fields['password'].label = False
        else:
            self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Vartotojo vardas'})
            self.fields['username'].label = False
            self.fields['password'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Slaptažodis'}) 
            self.fields['password'].label = False


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        language = get_language()
        if language == 'en':
            self.fields['username'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Username')})
            self.fields['email'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Email')})
            self.fields['password1'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Password')})        
            self.fields['password2'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Repeat password')})
        else:
            self.fields['username'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Vartotojo vardas')})
            self.fields['email'].widget.attrs.update({'class': 'form-control', 'placeholder': ('El paštas')})
            self.fields['password1'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Slaptažodis')})        
            self.fields['password2'].widget.attrs.update({'class': 'form-control', 'placeholder': ('Pakartokite')})

        

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'phone']

