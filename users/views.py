from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from wood.models import Order, User

def register(request):
    language = request.LANGUAGE_CODE
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            if language == 'lt':
                messages.success(request, f'Paskyra sukurtą {username} vartotojui! Dabar galite prisijungti!')
            else:
                messages.success(request, f'Account for {username} is created! You can now log in!')
            return redirect('login')
        else:
            if language == 'lt':
                messages.warning(request, 'Blogai suvesti duomenys, bandykite dar kartą!')
            else:
                messages.warning(request, 'The data inserted was incorrect, please try again!')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    language = request.LANGUAGE_CODE
    username = request.user.username
    user_order = Order.objects.filter(user__username=username).order_by('-id')
    count = Order.objects.filter(user__username=username).count()
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            if language == 'lt':
                messages.success(request, 'Paskyra pakeista!')
            if language == 'en':
                messages.success(request, 'Your account has been changed!')
            return redirect('profile')
        if not p_form.is_valid():
            if language == 'lt':
                messages.warning(request, 'Įveskite teisiklingą telefono numerį! Pvz: "+37063357079"')
            if language == 'en':
                messages.warning(request, 'Please insert a valid phone number! Example: "+37063357079"')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
    context = {
        'u_form': u_form,
        'p_form': p_form,
        'user_order': user_order,
        'count': count
    }
    return render(request, 'users/profile.html', context)



def phone(request):
    language = request.LANGUAGE_CODE
    if request.method == 'POST':
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if p_form.is_valid():
            p_form.save()
            if language == 'lt':
                messages.success(request, 'Telefono numeris pridėtas!')
            if language == 'en':
                messages.success(request, 'Phone number has been added!')
            return redirect('wood-home')
        if not p_form.is_valid():
            if language == 'lt':
                messages.warning(request, 'Įveskite teisiklingą telefono numerį! Pvz: "+37063357079"')
            if language == 'en':
                messages.warning(request, 'Please insert a valid phone number! Example: "+37063357079"')
    else:
        p_form = ProfileUpdateForm(instance=request.user.profile)
    context = {
        'p_form': p_form
    }
    return render(request, 'users/phone.html', context)